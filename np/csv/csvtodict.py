import csv

def csv_list(filePath, fieldNames=None, unicode=False, tab=False):
    # Select between tab or comma (default) separated values
    if tab:
        dialect = csv.excel_tab
    else:
        dialect = csv.excel
    f = open(filePath, 'r')
    # If fieldNames has been given then return a list of dicts
    if fieldNames:
        data = list(csv.DictReader(f, fieldNames, dialect=dialect))
    else: # Read in the data from a tab separated file into a list (of lists)
        data = list(csv.reader(f, dialect=dialect))
    # Convert all strings to unicode
    if unicode:
        for row in data:
            if type(row) is dict:
                for key, value in row.items():
                    if not type(value) is unicode: 
                        row[key] = value and value.decode('UTF-8')
            else:
                for i, item in enumerate(row):
                    if not type(item) is unicode: 
                        row[i] = item.decode('UTF-8')
    f.close()
    return data