from simpletal import simpleTALES, simpleTALUtils

# This object caches templates
templateCache = simpleTALUtils.TemplateCache()

def fill_template(filePath, substitutions):
    template = templateCache.getXMLTemplate(filePath)
    context = simpleTALES.Context(allowPythonPath=1)
    for key, value in substitutions.items():
        context.addGlobal(key, value)
    fastStringIO = simpleTALUtils.FastStringOutput()
    template.expand(context, fastStringIO, outputEncoding='UTF-8', docType='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">', suppressXMLDeclaration=True)
    return fastStringIO.getvalue()
