class WordMatcher:
    def __init__(self, filePath):
        self.wordList = file(filePath).readlines()
    
    def returnMatches(self, wordpart, limit=0):
        matchList = []
        limited = False
        for word in self.wordList:
            if word.startswith(wordpart):
                matchList.append(word)
                if len(matchList) == limit:
                    limited = True
                    break
        return matchList, limited
    