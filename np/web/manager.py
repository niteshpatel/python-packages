from pythoncom import CoInitialize, CoUninitialize
from np.web.pagemodule import page_module

def process_form(form, config):
    # If the next page has been specified immediately go to it
    nextPageID = form.get('next_page_id')
    results = {}
    if not nextPageID:
        # Determine the page to be processed and process the results
        prevPage = page_module(config, form['page_id'])
        results = prevPage.process_form(config, form)
        nextPageID = results['next_page_id']
    # Determine the next page to show and then create it
    nextPage = page_module(config, nextPageID)
    data = nextPage.create_page(config, form, results)
    return data

def safe_process_form(*args):
    CoInitialize()
    result = process_form(*args)
    CoUninitialize()
    return result
