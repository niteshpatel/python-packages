from np.web.manager import safe_process_form
from mod_python import apache
from mod_python import util

def get_request(request):
    form = {}
    for key in request.keys():
        form[key] = request.getlist(key)
        if isinstance(form[key], list):
            if len(form[key]) == 1:
                form[key] = form[key][0]
    return form

def handler(req):
    # Determine the config file to be used
    options = req.get_options()
    exec 'import %s.config as config' % options['package']
    # Get the form contents
    cgiForm = util.FieldStorage(req)
    form = get_request(cgiForm)
    data = safe_process_form(form, config)
    req.content_type = "text/html; charset=UTF-8"
    req.write(data)
    return apache.OK