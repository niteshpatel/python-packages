from mod_python import apache, util

def get_request(request):
    form = {}
    for key in request.keys():
        form[key] = request.getlist(key)
        if isinstance(form[key], list):
            if len(form[key]) == 1:
                form[key] = form[key][0]
    return form

def get_data(package, module, function, **form):
    exec 'from %s.%s import %s' % (package, module, function)
    return eval(function)(form)

def handler(req):
    try:
        req.content_type = "text/html; charset=UTF-8"
        req.write(get_data(req.get_options()['package'], **get_request(util.FieldStorage(req))))
        return apache.OK
    except (ImportError, TypeError):
        raise #'ProcessorError'
    