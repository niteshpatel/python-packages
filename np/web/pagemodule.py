def page_module(config, pageID):
    exec 'from %s.pages import %s' % (config.PACKAGE_NAME, pageID)
    return eval(pageID)
