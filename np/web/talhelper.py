import os

from np.tal.utils import fill_template


def fill_page(modulePath, substitutions, extension='html'):
    packageName, pagePackage, pageID = modulePath.split('.')[-3:]
    exec 'from %s.config import TEMPLATE_DIR' % packageName
    fileName = '%s.%s' % (pageID, extension)
    filePath = os.path.join(TEMPLATE_DIR, fileName)
    substitutions['page_id'] = pageID
    return fill_template(filePath, substitutions)
    