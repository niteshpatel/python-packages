import os

USER_CONF = r'C:\Program Files\fop-0.91beta\conf\userconfig.xml'

def create_xmlfopdf(xmlPath, xsltPath, pdfPath):
    strCmd = 'fop -c "%s" -xml "%s" -xsl "%s" "%s"' % (USER_CONF, xmlPath, xsltPath, pdfPath)
    return os.system(strCmd)
