import Pyro.core
import Pyro.naming
from np.xml.xmlfopdf import create_xmlfopdf

class RemoteTask(Pyro.core.ObjBase):
    def connect_test(self):
        pass
    def do_task(self, *args, **kwargs):
        return create_xmlfopdf(*args, **kwargs)

if __name__ == '__main__':
    Pyro.core.initServer(0)
    daemon = Pyro.core.Daemon()
    uri = daemon.connect(RemoteTask(), name="xmlfopdf")
    daemon.requestLoop()
