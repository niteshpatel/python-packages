from xml.dom import minidom

def xml_to_dict(xmlPath, singles=[]):
    xmlDoc = minidom.parse(xmlPath)
    return element_to_dict(xmlDoc.documentElement, singles=singles)

def element_to_dict(element, singles=[]):
    if element.childNodes.length == 1:
        if element.firstChild.nodeType == 3:
            return element.firstChild.nodeValue
    data = {}
    for child in element.childNodes:
        if child.nodeName in singles and child.nodeType == 3:
            data[child.nodeName] = child.firstChild.nodeValue
        else:
            if child.nodeType != 3:
                if not data.has_key(child.nodeName):
                    data[child.nodeName] = []
                childData = element_to_dict(child, singles=singles)
                if childData:
                    if child.nodeName in singles:
                        data[child.nodeName] = childData
                    else:
                        data[child.nodeName].append(childData)
    return data
