from xml.dom import minidom
from pyRXP import Parser 

PYRXP_NODE_NAME = 0
PYRXP_NODE_ATTRIBUTES = 1
PYRXP_NODE_CHILDREN = 2
NP_NODE_NAME = 0
NP_NODE_CHILDREN = 1

def lists_to_xml(lists):
    xmlDoc = minidom.Document()
    xmlDoc.appendChild(_lists_to_node(xmlDoc, lists))
    return xmlDoc.toxml()
    
def _lists_to_node(xmlDoc, lists):
    nodeContent = lists[NP_NODE_CHILDREN]
    xmlNode = xmlDoc.createElement(lists[NP_NODE_NAME])
    if isinstance(nodeContent, (list, tuple)):
        for item in nodeContent:
            xmlNode.appendChild(_lists_to_node(xmlDoc, item))
    else:
        xmlNode.appendChild(xmlDoc.createTextNode(nodeContent))
    return xmlNode

def xml_to_lists(xmlTxt):
    parser = Parser()
    tuples = parser.parse(xmlTxt)
    return _tuples_to_lists([tuples])[0]

def _tuples_to_lists(tuples):
    # If the child is text (we assume that a node only contains
    # text or elements but not both)
    if (len(tuples) == 1) and (isinstance(tuples[0], (str, unicode))):
        return tuples[0]
    else:
        lists = []
        for element in tuples:
            # We ignore text nodes when mixed with element nodes
            if not isinstance(element, (str, unicode)):
                lists.append([
                    element[PYRXP_NODE_NAME],
                    _tuples_to_lists(element[PYRXP_NODE_CHILDREN])])
    return lists
    
if __name__ == '__main__':
    from pprint import pprint
    if 0:
        lists = [
            'root', [
                [
                    'kanji',
                    [
                        [
                            'character',
                            'hello'
                            ]
                        ]
                    ],
                [
                    'kanji',
                    [
                        [
                            'character',
                            'world'
                            ]
                        ]
                    ],
                [
                    'kanji',
                    [
                        [
                            'character',
                            'mate'
                            ]
                        ]
                    ]
                ]
            ]
        xmlDoc = lists_to_xml(lists)
        xmlText = xmlDoc.toxml()
        print xmlText
    if 1:
        path = r'C:\Inetpub\wwwroot\japanese\resources\kanjitest.xml'
        xmlTxt = file(path, 'r').read()
        pprint(xml_to_lists(xmlTxt))